# grafana-tiles

Render a dashboard-JSON as Markdown or reStructuredText with image tiles.

Large and complex Grafana dashboards need additional information. Panels
have description or info-text and some of them even have catchy titles.
But what if you have more to tell or already have a (corporate)
documentation system?

```
java  -jar target/grafana-tiles-1.0.0-SNAPSHOT.jar -m -r -o samples ~/Downloads/Grafana\ Play\ Home-1581020701678.json
```

The command will write a .rst and a .md-file along with a index-tile for each panel of the (Grafana-) JSON.

See /samples for static output.

# How to Build

1. get a decent JDK, everything from 11 on will do
1. get Maven 3.6 or later
1. `mvn clean install`
1. the command from above

# Planned

* also offer SVG
* make colors configurable
* make sizes configurable
* fetch screenshots through Grafana-API
* make it a Jigsaw-thing/ executable chunk with jlink