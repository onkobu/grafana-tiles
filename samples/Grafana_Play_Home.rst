
================
Dashboard Panels
================

.. toctree::
    :maxdepth: 1

Feature showcases
-----------------

Data Source: 


.. image:: Grafana_Play_Home_7.png
    :width: 146px
    :alt: Position in Dashboard

Data source demos
-----------------

Data Source: 


.. image:: Grafana_Play_Home_9.png
    :width: 146px
    :alt: Position in Dashboard

What's New
----------

Data Source: 


.. image:: Grafana_Play_Home_8.png
    :width: 146px
    :alt: Position in Dashboard

server requests
---------------

Data Source: 


.. image:: Grafana_Play_Home_2.png
    :width: 146px
    :alt: Position in Dashboard

Memory / CPU
------------

Data Source: 


.. image:: Grafana_Play_Home_4.png
    :width: 146px
    :alt: Position in Dashboard

client side full page load
--------------------------

Data Source: 


.. image:: Grafana_Play_Home_5.png
    :width: 146px
    :alt: Position in Dashboard

logins
------

Data Source: 


.. image:: Grafana_Play_Home_3.png
    :width: 146px
    :alt: Position in Dashboard

Traffic In/Out
--------------

Data Source: graphite


.. image:: Grafana_Play_Home_11.png
    :width: 146px
    :alt: Position in Dashboard

