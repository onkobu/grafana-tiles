The JSON was taken from https://play.grafana.org. All headings are rendered from the JSON. Each image represents a panel's position on the dashboard.

# Feature showcases

Data Source: 



![Position in Dashoard](./Grafana_Play_Home-1581020701678_7.png)

# Data source demos

Data Source: 



![Position in Dashoard](./Grafana_Play_Home-1581020701678_9.png)

# What's New

Data Source: 



![Position in Dashoard](./Grafana_Play_Home-1581020701678_8.png)

# server requests

Data Source: 



![Position in Dashoard](./Grafana_Play_Home-1581020701678_2.png)

# Memory / CPU

Data Source: 



![Position in Dashoard](./Grafana_Play_Home-1581020701678_4.png)

# client side full page load

Data Source: 



![Position in Dashoard](./Grafana_Play_Home-1581020701678_5.png)

# logins

Data Source: 



![Position in Dashoard](./Grafana_Play_Home-1581020701678_3.png)

# Traffic In/Out

Data Source: graphite



![Position in Dashoard](./Grafana_Play_Home-1581020701678_11.png)

