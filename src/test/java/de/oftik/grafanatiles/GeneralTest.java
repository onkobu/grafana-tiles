package de.oftik.grafanatiles;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.time.format.DateTimeFormatterBuilder;
import java.time.temporal.ChronoField;

import org.hamcrest.MatcherAssert;
import org.hamcrest.Matchers;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.ValueSource;

class GeneralTest {
	private static final DateTimeFormatter FORMATTER = new DateTimeFormatterBuilder()
			.append(DateTimeFormatter.ISO_LOCAL_DATE)//
			.appendLiteral(' ').appendPattern("HH:mm:ss")//
			.appendFraction(ChronoField.NANO_OF_SECOND, 0, 9, true)//
			.toFormatter();

	@ParameterizedTest
	@ValueSource(strings = { "2014-05-12 16:00:28.45", "2014-05-12 16:00:28", "2014-05-12 16:00:28.1",
			"2014-05-12 16:00:28.000000000", "2014-05-12 16:00:28.1000000", "2014-05-12 16:00:28.12345678",
			"2014-05-12 16:00:28.1234567", "2014-05-12 16:00:28.123456", "2014-05-12 16:00:28.12345",
			"2014-05-12 16:00:28.1234", "2014-05-12 16:00:28.123" })
	void whenStringCorrect_thenParsingOk(String str) {
		MatcherAssert.assertThat(LocalDateTime.parse(str, FORMATTER), Matchers.notNullValue());
	}
}
