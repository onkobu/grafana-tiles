package de.oftik.grafanatiles;

public enum Message {
	UNREADABLE_FILE("Cannot read file %s%n"),

	NO_FILES_TO_PROCESS("No files left to process%n"),

	JSON_ERROR("Error reading file %s%n");

	private final String message;

	Message(String msg) {
		this.message = msg;
	}

	public String message() {
		return message;
	}
}
