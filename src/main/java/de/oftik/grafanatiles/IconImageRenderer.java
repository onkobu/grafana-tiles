package de.oftik.grafanatiles;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;

import javax.imageio.ImageIO;

import de.oftik.grafanatiles.RenderContext.ResourceType;

public class IconImageRenderer implements Renderer {
	// https://grafana.com/docs/grafana/latest/reference/dashboard/
	private static final int COLUMNS_IN_GRAFANA = 24;

	private static final int TILE_WIDTH_FACTOR = 6;
	private static final int TILE_HEIGHT_FACTOR = 14;

	public static final int ICON_WIDTH = COLUMNS_IN_GRAFANA * TILE_WIDTH_FACTOR + 2;

	@Override
	public String label() {
		return "Icon";
	}

	@Override
	public void render(RenderContext ctx, GrafanaFile file) {
		final int rowCount = file.getRowCount();
		final int totalHeight = (rowCount + 1) * TILE_HEIGHT_FACTOR + 2;
		final BufferedImage baseImage = createBaseImage(rowCount, totalHeight, ICON_WIDTH);
		for (GrafanaPanel panel : file.getPanels()) {
			final BufferedImage bufferedImage = copyImage(baseImage);
			final Graphics g = bufferedImage.getGraphics();

			g.setColor(new Color(136, 137, 182));
			g.fillRoundRect(panel.getX() * TILE_WIDTH_FACTOR, panel.getRow() * TILE_HEIGHT_FACTOR,
					panel.getWidth() * TILE_WIDTH_FACTOR, TILE_HEIGHT_FACTOR, 2, 2);
			final File imgFile = ctx.determineOutputFile("_" + panel.getId() + ".png");
			try {
				ImageIO.write(bufferedImage, "png", imgFile);
				ctx.registerResource(panel, imgFile, ResourceType.TILE_INDEX_IMAGE);
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
	}

	private BufferedImage createBaseImage(final int rowCount, final int totalHeight, final int totalWidth) {
		final BufferedImage baseImage = new BufferedImage(totalWidth, totalHeight, BufferedImage.TYPE_INT_ARGB);
		final Graphics baseGraphics = baseImage.getGraphics();

		baseGraphics.setColor(Color.BLACK);
		baseGraphics.drawRoundRect(0, 0, 24 * TILE_WIDTH_FACTOR, (rowCount + 1) * TILE_HEIGHT_FACTOR, 3, 3);
		for (int col = 0; col < COLUMNS_IN_GRAFANA; col++) {
			if (col % 2 == 0) {
				baseGraphics.setColor(Color.GRAY);
			} else {
				baseGraphics.setColor(Color.LIGHT_GRAY);
			}
			baseGraphics.drawLine(col * TILE_WIDTH_FACTOR, 0, col * TILE_WIDTH_FACTOR, totalHeight);
		}
		for (int row = 0; row <= rowCount; row++) {
			baseGraphics.drawLine(0, row * TILE_HEIGHT_FACTOR, totalWidth, row * TILE_HEIGHT_FACTOR);
		}
		return baseImage;
	}

	public static BufferedImage copyImage(BufferedImage source) {
		BufferedImage b = new BufferedImage(source.getWidth(), source.getHeight(), source.getType());
		Graphics2D g = (Graphics2D) b.getGraphics();
		g.drawImage(source, 0, 0, null);
		g.dispose();
		return b;
	}

}
