package de.oftik.grafanatiles;

import java.io.BufferedWriter;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;

public class RenderContext {
	public enum ResourceType {
		TILE_INDEX_IMAGE;
	}

	public static class OrException {
		private static final OrException EMPTY = new OrException() {

			@Override
			public void exitOnError() {
			}
		};

		private final Exception exception;
		private OrException consecutiveError;

		private List<OrException> consecutiveErrors;

		private OrException() {
			this.exception = null;
		}

		public OrException(Exception e) {
			this.exception = e;
		}

		public Exception getException() {
			if (consecutiveError == null && (consecutiveErrors == null || consecutiveErrors.isEmpty())) {
				return exception;
			}

			if (consecutiveError != null) {
				exception.addSuppressed(consecutiveError.getException());
			}

			if (consecutiveErrors != null) {
				consecutiveErrors.stream().map(OrException::getException).forEach(exception::addSuppressed);
			}
			return exception;
		}

		public OrException or(OrException ex) {
			consecutiveError = ex;
			return ex;
		}

		public OrException or(List<OrException> others) {
			consecutiveErrors = others;
			return this;
		}

		public void exitOnError() {
			if (getException() == null) {
				return;
			}
			getException().printStackTrace();
		}

		public static OrException none() {
			return EMPTY;
		}

		public static void exitOnError(OrException ex) {
			if (ex == EMPTY) {
				return;
			}
			ex.exception.printStackTrace();
			System.exit(17);
		}
	}

	public static class ResourceWrapper {
		private final ResourceType type;
		private final Object value;

		public ResourceWrapper(ResourceType type, Object value) {
			super();
			this.type = type;
			this.value = value;
		}

		public ResourceType getType() {
			return type;
		}

		public Object getValue() {
			return value;
		}

		public OrException writeTo(BufferedWriter writer) {
			switch (type) {
			case TILE_INDEX_IMAGE: {
				try {
					final File f = (File) getValue();
					writer.write(String.valueOf(f.getName()));
					return OrException.none();
				} catch (IOException e) {
					return new OrException(e);
				}
			}
			default: {
				try {
					writer.write(String.valueOf(getValue()));
					return OrException.none();
				} catch (IOException e) {
					return new OrException(e);
				}
			}
			}
		}
	}

	private final Map<GrafanaPanel, List<ResourceWrapper>> panelResourceMap = new HashMap<>();
	private File outputDirectory;
	private File currentDashboardFile;

	public void registerResource(GrafanaPanel panel, Object o, ResourceType type) {
		final List<ResourceWrapper> resources;
		if (!panelResourceMap.containsKey(panel)) {
			resources = new ArrayList<>();
			panelResourceMap.put(panel, resources);
		} else {
			resources = panelResourceMap.get(panel);
		}
		resources.add(new ResourceWrapper(type, o));
	}

	public Optional<ResourceWrapper> getResource(GrafanaPanel p, ResourceType type) {
		return panelResourceMap.get(p).stream().filter(w -> w.getType() == type).findFirst();
	}

	public String getOutputDirectory() {
		return outputDirectory.getAbsolutePath();
	}

	public void setOutputDirectory(String outDir) {
		outputDirectory = new File(outDir);
	}

	public File determineOutputFile(String suffix) {
		if (outputDirectory == null) {
			return outputRelativeToDashboard(suffix);
		}
		return outputToOutputDirectory(suffix);
	}

	private File outputToOutputDirectory(String suffix) {
		if (!outputDirectory.isDirectory() && outputDirectory.exists()) {
			throw new IllegalArgumentException("cannot write to non directory output " + outputDirectory);
		}
		if (!outputDirectory.exists()) {
			outputDirectory.mkdirs();
		}
		return new File(outputDirectory, replaceSpaces(currentDashboardFile.getName()).replace(".json", suffix));
	}

	private String replaceSpaces(String name) {
		return name.replace(" ", "_");
	}

	private File outputRelativeToDashboard(String suffix) {
		File file = new File(replaceSpaces(currentDashboardFile.getAbsolutePath()).replace(".json", suffix));
		return file;
	}

	public void setCurrentDashboardFile(File f) {
		currentDashboardFile = f;
	}
}
