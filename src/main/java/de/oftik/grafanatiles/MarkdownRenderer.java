package de.oftik.grafanatiles;

import java.io.BufferedWriter;
import java.io.File;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.util.Optional;
import java.util.stream.Collectors;

import de.oftik.grafanatiles.RenderContext.OrException;
import de.oftik.grafanatiles.RenderContext.ResourceType;

public class MarkdownRenderer implements Renderer {
	private BufferedWriter writer;
	private RenderContext context;

	@Override
	public void render(RenderContext ctx, GrafanaFile file) {
		final File markdownFile = ctx.determineOutputFile(".md");
		try (BufferedWriter br = Files.newBufferedWriter(markdownFile.toPath(), StandardCharsets.UTF_8)) {
			writer = br;
			context = ctx;
			final OrException orEx = writeIfNotEmpty(file.getDescription())
					.or(file.getPanels().stream().map(this::writePanelSection).collect(Collectors.toList()));
			orEx.exitOnError();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	@Override
	public String label() {
		return "Markdown";
	}

	private OrException writeIfNotEmpty(String property) {
		if (property == null || property.isBlank()) {
			return OrException.none();
		}
		try {
			writer.write(property);
			return OrException.none();
		} catch (IOException e) {
			return new OrException(e);
		}
	}

	private OrException writePanelSection(GrafanaPanel p) {
		try {
			writer.write("# ");
			writer.write(p.getTitle());
			writer.write("\n\nData Source: ");
			writeIfNotEmpty(p.getDatasource());
			writer.write("\n\n");
			writeIfNotEmpty(p.getDescription());
			writer.write("\n\n![Position in Dashoard](./");
		} catch (IOException ex) {
			return new OrException(ex);
		}
		final Optional<OrException> orEx = context.getResource(p, ResourceType.TILE_INDEX_IMAGE)
				.map(rw -> rw.writeTo(writer));

		try {
			writer.write(")\n\n");
			return orEx.orElse(OrException.none());
		} catch (IOException e) {
			return new OrException(e);
		}
	}
}
