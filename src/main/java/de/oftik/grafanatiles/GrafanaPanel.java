package de.oftik.grafanatiles;

import java.util.Map;

import com.fasterxml.jackson.annotation.JsonProperty;

public class GrafanaPanel {
	// row
	private static final int HEIGHT_UNITS = 5;

	private int id;
	private String title;
	private int height;
	private int width;
	private int x;
	private int y;
	private int row;
	private String datasource;
	private String description;

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getDatasource() {
		return datasource;
	}

	public void setDatasource(String datasource) {
		this.datasource = datasource;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	@JsonProperty("gridPos")
	void unpackNested(Map<String, Object> brand) {
		this.height = Integer.parseInt(String.valueOf(brand.get("h")));
		this.width = Integer.parseInt(String.valueOf(brand.get("w")));
		this.x = Integer.parseInt(String.valueOf(brand.get("x")));
		this.y = Integer.parseInt(String.valueOf(brand.get("y")));
		this.row = this.y / HEIGHT_UNITS;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public int getHeight() {
		return height;
	}

	public void setHeight(int height) {
		this.height = height;
	}

	public int getWidth() {
		return width;
	}

	public void setWidth(int width) {
		this.width = width;
	}

	public int getX() {
		return x;
	}

	public void setX(int x) {
		this.x = x;
	}

	public int getY() {
		return y;
	}

	public void setY(int y) {
		this.y = y;
	}

	public int getRow() {
		return row;
	}

	public void setRow(int row) {
		this.row = row;
	}
}
