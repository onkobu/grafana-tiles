package de.oftik.grafanatiles;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

public class ModularRenderer implements Renderer {
	private final List<Renderer> renderers;

	private final RenderContext ctx;

	public ModularRenderer(String outDir) {
		super();
		this.ctx = new RenderContext();
		ctx.setOutputDirectory(outDir);

		renderers = new ArrayList<>();
		renderers.add(new IconImageRenderer());
	}

	@Override
	public String label() {
		return "Modular";
	}

	public void render(File f) {
		final GrafanaFile gf = GrafanaFile.readFrom(f);
		if (!gf.isOk()) {
			System.err.printf(Message.JSON_ERROR.message(), f);
			gf.getException().printStackTrace(System.err);
		}
		ctx.setCurrentDashboardFile(f);
		render(ctx, gf);
	}

	@Override
	public void render(RenderContext ctx, GrafanaFile file) {
		System.out.printf("rendering of %s, id %s, with %d panels in %d rows%n", file.getSourceFile(), file.getId(),
				file.getPanels().size(), file.getRowCount());
		renderers.forEach(r -> {
			System.out.println("\t" + r.label());
			r.render(ctx, file);
		});
	}

	public void enableMarkdown(boolean renderMarkdown) {
		if (renderMarkdown) {
			renderers.add(new MarkdownRenderer());
		}
	}

	public void enableRestructuredText(boolean renderRst) {
		if (renderRst) {
			renderers.add(new ReStructuredTextRenderer());
		}
	}
}
