package de.oftik.grafanatiles;

import static de.oftik.grafanatiles.IconImageRenderer.ICON_WIDTH;

import java.io.BufferedWriter;
import java.io.File;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.util.Optional;
import java.util.stream.Collectors;

import de.oftik.grafanatiles.RenderContext.OrException;
import de.oftik.grafanatiles.RenderContext.ResourceType;

public class ReStructuredTextRenderer implements Renderer {
	private static final String HEADING_TEMPLATE = "--------------------------------------------------------------------------------------";

	private BufferedWriter writer;
	private RenderContext context;

	@Override
	public void render(RenderContext ctx, GrafanaFile file) {
		final File markdownFile = ctx.determineOutputFile(".rst");
		try (BufferedWriter br = Files.newBufferedWriter(markdownFile.toPath(), StandardCharsets.UTF_8)) {
			writer = br;
			context = ctx;
			writer.write("\n================\n");
			writer.write("Dashboard Panels\n");
			writer.write("================\n\n");
			writer.write(".. toctree::\n    :maxdepth: 1\n\n");
			final OrException orEx = writeIfNotEmpty(file.getDescription())
					.or(file.getPanels().stream().map(this::writePanelSection).collect(Collectors.toList()));
			orEx.exitOnError();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	private OrException writeIfNotEmpty(String property) {
		if (property == null || property.isBlank()) {
			return OrException.none();
		}
		try {
			writer.write(property);
			return OrException.none();
		} catch (IOException e) {
			return new OrException(e);
		}
	}

	private OrException writePanelSection(GrafanaPanel p) {
		try {

			writer.write(p.getTitle());
			writer.write("\n");
			writer.write(headingFor(p.getTitle()));
			writer.write("\n");
			writer.write("\nData Source: ");
			writeIfNotEmpty(p.getDatasource());
			writer.write("\n");
			writeIfNotEmpty(p.getDescription());
			writer.write("\n\n.. image:: ");
		} catch (IOException ex) {
			return new OrException(ex);
		}
		final Optional<OrException> orEx = context.getResource(p, ResourceType.TILE_INDEX_IMAGE)
				.map(rw -> rw.writeTo(writer));

		try {
			writer.write("\n    :width: ");
			writer.write(String.valueOf(ICON_WIDTH));
			writer.write("px\n    :alt: Position in Dashboard\n\n");
			return orEx.orElse(OrException.none());
		} catch (IOException e) {
			return new OrException(e);
		}
	}

	private String headingFor(String title) {
		if (title.length() > HEADING_TEMPLATE.length()) {
			final StringBuilder sb = new StringBuilder(HEADING_TEMPLATE);
			for (int i = 0; i <= HEADING_TEMPLATE.length() / title.length(); i++) {
				sb.append(HEADING_TEMPLATE);
			}
			return sb.substring(0, title.length());
		}
		return HEADING_TEMPLATE.substring(0, title.length());
	}

	@Override
	public String label() {
		return "reStructuredText";
	}
}
