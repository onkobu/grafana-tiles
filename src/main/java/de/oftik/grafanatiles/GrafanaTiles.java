package de.oftik.grafanatiles;

import java.io.File;
import java.util.List;
import java.util.stream.Collectors;

import org.apache.commons.cli.CommandLine;
import org.apache.commons.cli.CommandLineParser;
import org.apache.commons.cli.DefaultParser;
import org.apache.commons.cli.HelpFormatter;
import org.apache.commons.cli.Option;
import org.apache.commons.cli.Options;

public class GrafanaTiles {
	private static final Options OPTIONS = new Options()
			.addOption(Option.builder("h").longOpt("help").desc("show help").build())
			.addOption(Option.builder("o").longOpt("output").hasArg(true).desc("Output directory").build())
			.addOption(Option.builder("r").longOpt("rst").desc("enable ReStructured Text").build())
			.addOption(Option.builder("m").longOpt("md").desc("enable Markdown Text").build());

	private final List<File> jsonFiles;

	private String outputDirectory;

	private boolean renderMarkdown;

	private boolean renderRst;

	public GrafanaTiles(List<String> jsonFileNames) {
		final List<File> allFiles = jsonFileNames.stream().map(File::new).collect(Collectors.toList());
		this.jsonFiles = allFiles.stream().filter(f -> f.exists() && f.isFile() && f.canRead())
				.collect(Collectors.toList());
		allFiles.removeAll(jsonFiles);
		allFiles.forEach(f -> System.out.printf(Message.UNREADABLE_FILE.message(), f));
	}

	public static void main(String[] args) throws Exception {
		final CommandLineParser parser = new DefaultParser();
		final CommandLine cmd = parser.parse(OPTIONS, args);

		if (args.length == 0 || cmd.hasOption("h")) {
			printUsageAndExit();
		}

		final GrafanaTiles grafanaTiles = new GrafanaTiles(cmd.getArgList());
		if (cmd.hasOption("o")) {
			grafanaTiles.setOutputDirectory(cmd.getOptionValue("o"));
		}

		grafanaTiles.setRenderMarkdown(cmd.hasOption("m"));
		grafanaTiles.setRenderRst(cmd.hasOption("r"));
		grafanaTiles.processReadable();
	}

	private void setRenderRst(boolean hasOption) {
		renderRst = hasOption;
	}

	private void setRenderMarkdown(boolean hasOption) {
		renderMarkdown = hasOption;
	}

	private void setOutputDirectory(String optionValue) {
		outputDirectory = optionValue;
	}

	private void processReadable() {
		if (jsonFiles.isEmpty()) {
			System.out.printf(Message.NO_FILES_TO_PROCESS.message());
		}

		final ModularRenderer mr = new ModularRenderer(outputDirectory);
		mr.enableMarkdown(renderMarkdown);
		mr.enableRestructuredText(renderRst);
		jsonFiles.forEach(mr::render);
	}

	private static void printUsageAndExit() {
		HelpFormatter formatter = new HelpFormatter();
		formatter.printHelp("grafana-tiles <json-files>", OPTIONS);
		System.exit(0);
	}
}
