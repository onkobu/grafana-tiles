package de.oftik.grafanatiles;

import java.io.File;
import java.io.IOException;
import java.util.Comparator;
import java.util.List;

import com.fasterxml.jackson.annotation.JacksonInject;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.InjectableValues;
import com.fasterxml.jackson.databind.InjectableValues.Std;
import com.fasterxml.jackson.databind.ObjectMapper;

public class GrafanaFile {
	private static final ObjectMapper OBJECT_MAPPER = new ObjectMapper()
			.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);

	private static final Comparator<GrafanaPanel> ROW_COMPARATOR = new Comparator<GrafanaPanel>() {
		@Override
		public int compare(GrafanaPanel o1, GrafanaPanel o2) {
			return o1.getRow() - o2.getRow();
		}
	};

	private static final GrafanaPanel EMPTY_PANEL = new GrafanaPanel();

	private final IOException exception;

	private String description;

	private String id;

	@JacksonInject("sourceFile")
	private File sourceFile;

	private List<GrafanaPanel> panels;

	public GrafanaFile() {
		this.exception = null;
	}

	public File getSourceFile() {
		return sourceFile;
	}

	public void setSourceFile(File sourceFile) {
		this.sourceFile = sourceFile;
	}

	public GrafanaFile(IOException e) {
		this.exception = e;
	}

	public boolean isOk() {
		return exception == null;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public IOException getException() {
		return exception;
	}

	public List<GrafanaPanel> getPanels() {
		return panels;
	}

	public void setPanels(List<GrafanaPanel> panels) {
		this.panels = panels;
	}

	public static GrafanaFile readFrom(File grafanaFile) {
		final Std injectables = new InjectableValues.Std().addValue("sourceFile", grafanaFile).addValue(File.class,
				grafanaFile);
		OBJECT_MAPPER.setInjectableValues(injectables);
		try {
			return OBJECT_MAPPER.reader(injectables).forType(GrafanaFile.class).readValue(grafanaFile);
		} catch (IOException e) {
			return new GrafanaFile(e);
		}
	}

	public int getRowCount() {
		return panels.stream().max(ROW_COMPARATOR).orElse(EMPTY_PANEL).getRow();
	}

}
