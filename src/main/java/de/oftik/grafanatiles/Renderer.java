package de.oftik.grafanatiles;

public interface Renderer {
	String label();

	void render(RenderContext ctx, GrafanaFile file);
}
